package com.example.demo.repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Port;
import com.example.demo.entity.Stock;
import com.example.demo.wrapper.StockWrapper;

import yahoofinance.YahooFinance;

@Repository
public class MYSQLStockRepository implements StockRepository {

	@Autowired
	private JdbcTemplate template;

	@Override
	public StockWrapper displayStockDetails(String ticker) throws IOException {
		return new StockWrapper(YahooFinance.get(ticker));
	}

	@Override
	public StockWrapper getStockById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockWrapper editStock(StockWrapper stockWrapper) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Stock> getAllStocks() {
		String sql = "SELECT * FROM history order by datetime DESC";
		return template.query(sql, new StockRowMapper());
	}
	@Override
	public List<Port> getAllPort() {
		String sql = "SELECT * FROM stockdetails order by volume*value DESC";
		return template.query(sql, new PortRowMapper());
	}
	@Override
	public Stock addStock(Stock stock) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		template.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO history(ticker, datetime, volume, value_per_stock, buy_or_sell) VALUES(?,?,?,?,?);"
								+ "INSERT INTO StockDetails(ticker, volume, value) VALUES (?,?,?) ON DUPLICATE KEY UPDATE volume=volume+?;",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, stock.getTicker());
				ps.setTimestamp(2, stock.getDateTime());
				ps.setInt(3, stock.getVolume());
				ps.setDouble(4, stock.getValuePerStock());
				ps.setString(5, stock.getBuyOrSell());
				ps.setString(6, stock.getTicker());
				ps.setInt(7, stock.getVolume());
				ps.setDouble(8, stock.getValuePerStock());
				ps.setInt(9, stock.getVolume());
				return ps;
			}
		}, keyHolder);

		stock.setOrderId(keyHolder.getKey().intValue());
		return stock;
	}

	@Override
	public Stock sellStock(Stock stock) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		template.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO history(ticker, datetime, volume, value_per_stock, buy_or_sell) VALUES(?,?,?,?,?);"
								+ "UPDATE stockdetails SET volume = volume - ? WHERE ticker = ?",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, stock.getTicker());
				ps.setTimestamp(2, stock.getDateTime());
				ps.setInt(3, stock.getVolume());
				ps.setDouble(4, stock.getValuePerStock());
				ps.setString(5, stock.getBuyOrSell());
				ps.setInt(6, stock.getVolume());
				ps.setString(7, stock.getTicker());
				return ps;
			}
		}, keyHolder);

		stock.setOrderId(keyHolder.getKey().intValue());
		return stock;
	}
}

class StockRowMapper implements RowMapper<Stock> {

	@Override
	public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Stock(rs.getInt("order_ID"), rs.getString("ticker"), rs.getTimestamp("datetime"),
				rs.getInt("volume"), rs.getInt("value_per_stock"), rs.getString("buy_or_sell"));
	}

}
class PortRowMapper implements RowMapper<Port> {

	@Override
	public Port mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Port(rs.getString("ticker"),rs.getInt("volume"), rs.getInt("value"), rs.getInt("returns"));
	}

}
