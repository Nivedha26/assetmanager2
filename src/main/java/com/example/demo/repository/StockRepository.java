package com.example.demo.repository;

import java.io.IOException;
import java.util.List;

import com.example.demo.entity.Port;
import com.example.demo.entity.Stock;
import com.example.demo.wrapper.StockWrapper;

public interface StockRepository {
	public StockWrapper displayStockDetails(String ticker) throws IOException;

	public Stock addStock(Stock stock);

	public StockWrapper getStockById(int id);

	public StockWrapper editStock(StockWrapper stockWrapper);

	public List<Stock> getAllStocks();

	public List<Port> getAllPort();

	public Stock sellStock(Stock stock);

}
