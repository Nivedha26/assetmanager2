package com.example.demo.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Port;
import com.example.demo.entity.Stock;
import com.example.demo.service.StockService;
import com.example.demo.wrapper.StockWrapper;

@RestController
@EnableAutoConfiguration
@RequestMapping("api/stocks")

public class StockController {

	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

	@Autowired
	StockService stockService;

	@GetMapping
	public List<Stock> getAllShippers() throws IOException {
		return stockService.getAllStocks();
	}

	@GetMapping(value = "/buy_or_sell/{ticker}")
	@ResponseBody
	public String displayStockDetails(@PathVariable("ticker") String ticker) throws IOException {
		LOG.debug("getShipperById, id=[" + ticker + "]");
		StockWrapper stock = stockService.displayStockDetails(ticker);
		String symbol = stock.getStock().getSymbol();
		String name = stock.getStock().getName();
		BigDecimal price = stock.getStock().getQuote().getPrice();
		String stock_price = price.toString();
		return "symbol: " + symbol + "       name: " + name + "     price: " + stock_price + " " + LocalDateTime.now();
	}

	@GetMapping("/buy/{ticker}/{vol}")
	public Stock addStock(Stock stock, @PathVariable("ticker") String ticker, @PathVariable("vol") int vol) throws IOException {
		StockWrapper stockWrapper = stockService.displayStockDetails(ticker);

		stock = new Stock(0, ticker, Timestamp.valueOf(LocalDateTime.now()), vol,
				(stockWrapper.getStock().getQuote().getPrice()).doubleValue(), "buy");
		return stockService.addStock(stock);
	}

	@GetMapping("/sell/{ticker}/{vol}")
	public Stock sellStock(Stock stock, @PathVariable("ticker") String ticker,@PathVariable("vol") int vol) throws IOException {
		StockWrapper stockWrapper = stockService.displayStockDetails(ticker);

		stock = new Stock(0, ticker, Timestamp.valueOf(LocalDateTime.now()), vol,
				(stockWrapper.getStock().getQuote().getPrice()).doubleValue(), "sell");
		return stockService.sellStock(stock);
	}

	@GetMapping("/view/")
	public List<Stock> getAllStocks(Stock stock) throws IOException {
		return stockService.getAllStocks();
	}

	@GetMapping("/viewport/")
	public List<Port> getAllPort(Stock stock) throws IOException {
		return stockService.getAllPort();
	}
	/*
	 * @PostMapping public Stock addStock(@ResponseBody Stock stock) { return
	 * stockService.newStock(stock); }
	 */

	@RequestMapping("/testing")
	@ResponseBody
	public String testing() {
		return "hi";
	}

}

/* Stock stock = stockService.findStock("FB").getStock(); */

/*
 * BigDecimal stock_price = stock.getQuote().getPrice(); BigDecimal stock_yield
 * = stock.getDividend().getAnnualYieldPercent();
 */

/*
 * stock.getQuote() stock.getStats() stock.getDividend()
 * 
 * Use these methods to get more data.
 */

/* String price = stock_price.toString(); */

/*
 * @GetMapping("/")
 * 
 * @ResponseBody public BigDecimal home() throws IOException { return
 * stockService.findStock("AAPL").getStock().getQuote().getPrice(); }
 * 
 * @GetMapping("/Stocks")
 * 
 * @ResponseBody public String returnstock(@RequestParam String ticker) throws
 * IOException { Stock stock = stockService.findStock(ticker).getStock(); return
 * stock.toString(); }
 */
