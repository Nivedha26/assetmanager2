package com.example.demo.wrapper;

import java.time.LocalDateTime;

import yahoofinance.Stock;

public class StockWrapper {
    private final Stock stock;
    private final LocalDateTime lastAccess;
    
    public StockWrapper(Stock stock) {
        this.stock = stock;
        this.lastAccess = LocalDateTime.now();
    }

    public Stock getStock() {
        return stock;
    }
    
    public LocalDateTime getLastAccess() {
        return lastAccess;
    }

}