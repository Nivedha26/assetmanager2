package com.example.demo.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Port;
import com.example.demo.entity.Stock;
import com.example.demo.repository.StockRepository;
import com.example.demo.wrapper.StockWrapper;

@Service
public class StockService {

	@Autowired
	StockRepository stockRepository;

	public List<Stock> getAllStocks() throws IOException {
		return stockRepository.getAllStocks();
	}
	public List<Port> getAllPort() throws IOException{
		return stockRepository.getAllPort();
	}

	public StockWrapper displayStockDetails(String ticker) throws IOException {
		return stockRepository.displayStockDetails(ticker);
	}

	public Stock addStock(Stock stock) {
		return stockRepository.addStock(stock);
	}

	public Stock sellStock(Stock stock) {
		return stockRepository.sellStock(stock);
	}
}
