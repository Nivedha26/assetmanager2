package com.example.demo.entity;

public class Port {
	private String ticker;
	private int volume;
	private double valuePerStock;
    private int returns;
    public Port(String ticker,  int volume, double valuePerStock, int returns) {
		super();
		this.ticker = ticker;
		this.volume = volume;
		this.valuePerStock = valuePerStock;
        this.returns=returns;
	}
	
	public Port() { }
    public int getReturns() {
        return returns;
    }
    public String getTicker() {
        return ticker;
    }
    public double getValuePerStock() {
        return valuePerStock;
    }
    public int getVolume() {
        return volume;
    }
    public void setReturns(int returns) {
        this.returns = returns;
    }
    public void setTicker(String ticker) {
        this.ticker = ticker;
    }
    public void setVolume(int volume) {
        this.volume = volume;
    }
    public void setValuePerStock(double valuePerStock) {
        this.valuePerStock = valuePerStock;
    }
}
