CREATE DATABASE assetmanager;

USE assetmanager;

CREATE TABLE history
(
      order_ID INT NOT NULL AUTO_INCREMENT,
      ticker VARCHAR(10) NOT NULL,
      datetime DATETIME NOT NULL,
      volume INT NOT NULL,
      value_per_stock INT NOT NULL,
      buy_or_sell VARCHAR(4) NOT NULL,
      status_code INT DEFAULT 0 NOT NULL,
      PRIMARY KEY(order_ID)
);

CREATE TABLE stockdetails
(
      ticker VARCHAR(10) NOT NULL,
      volume INT NOT NULL,
      value INT NOT NULL,
      returns INT
);

SELECT * FROM history;
SELECT * FROM stockdetails;
